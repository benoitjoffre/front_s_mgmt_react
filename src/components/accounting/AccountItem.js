import React, { Fragment, useState, useContext } from 'react'
import '../../assets/sass/account.scss'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import AccountContext from '../../context/account/accountContext'
import {Modal, Button } from 'react-bootstrap'

const AccountItem = ({ account }) => {
    const accountContext = useContext(AccountContext)

    const { id, name } = account

    const { deleteAccount } = accountContext

    const onDelete = () => {
        deleteAccount(id)
        const handleClose = () => setShow(false);
        handleClose()
        // eslint-disable-next-line
    }

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

    return (
        <Fragment>
            <div key={account.id} className='card' style={{minWidth: '15rem', height: '10rem', margin: '15px'}}>
                <h5 className='text-dark text-center' style={{margin: '5px'}}>
                {name.toUpperCase()}
                </h5>
                <i onClick={handleShow} className="fas fa-trash-alt d-flex text-center" style={{margin:'auto'}}></i>

                <Modal show={show} onHide={handleClose}>
    				<Modal.Header closeButton>
    				<Modal.Title>Supprimer un budget</Modal.Title>
    				</Modal.Header>
    				
    				<Modal.Body>
    								<h4 className='text-center'>Êtes-vous sûr de vouloir supprimer ce budget ?</h4>
    				</Modal.Body>
    				<Modal.Footer>
    						<Button variant="secondary" onClick={handleClose}>
    								Annuler
    						</Button>
                            
    						<Button variant="danger" onClick={onDelete}>
    								Supprimer
    						</Button>
    				</Modal.Footer>
                </Modal>

                <Link to={'/mon-budget/'+id} className='button_open_account'>OUVRIR</Link>
            </div>
        </Fragment>
    )
}

AccountItem.propTypes = {
    account: PropTypes.object.isRequired
  };

export default AccountItem