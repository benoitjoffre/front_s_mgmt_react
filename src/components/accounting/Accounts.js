import React, { Fragment, useContext, useEffect } from 'react'
import '../../assets/sass/account.scss'
import AccountContext from '../../context/account/accountContext'
import AccountItem from './AccountItem'
import Spinner from '../layout/Spinner'


const Accounts = () => {
   
    const accountContext = useContext(AccountContext)

    const { accounts, getAccounts, loading, clearAccounts } = accountContext
   

    useEffect(() => {
        clearAccounts()
        getAccounts()
     // eslint-disable-next-line
    },[])

    if(accounts !== null && accounts.length === 0 && !loading) {
        return <h5 className='text-center' style={{marginTop: '50px'}}>Veuillez ajouter un budget</h5>
    }
    return (
        <Fragment>
            {accounts !== null && !loading ? ( 
            <div className='d-flex  align-content-start justify-content-around flex-wrap' style={{marginLeft: '50px', marginRight: '50px'}} >
                {accounts.map(account => 
                  <AccountItem key={account.id} account={account} />
                )}
            </div>
           
            ) : (
            <Spinner /> 
            )}
           
        </Fragment>
    )
}

export default Accounts