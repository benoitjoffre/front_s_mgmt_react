import React, { useState, useContext } from 'react'
import AccountContext from '../../context/account/accountContext'
import {Modal, Button } from 'react-bootstrap'

const AccountForm = () => {
	const accountContext = useContext(AccountContext)

	const { addAccount } = accountContext
    const [account, setAccount] = useState({
        name: ''
    });

		const { name } = account
		
		const onChange = e => setAccount({ ...account, [e.target.name]: e.target.value })

		const onSubmit = e => {
			e.preventDefault()
			addAccount(account)
			setAccount({ name: '' })
		}
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
		const handleShow = () => setShow(true);
		

    return (
        <>
            <Button variant="success" onClick={handleShow} style={{borderRadius: '100px', marginRight: '70px', marginTop: '50px'}}>
                Ajouter un budget
            </Button>

            <Modal show={show} onHide={handleClose}>
				<Modal.Header closeButton>
				<Modal.Title>Ajouter un budget</Modal.Title>
				</Modal.Header>
					<form onSubmit={onSubmit}>
						<Modal.Body>
							<input 
							className='form-control'
							type='text' 
							placeholder='Nom du budget'
							name='name'
							value={name}
							onChange={onChange}
							/>
						</Modal.Body>
						<Modal.Footer>
							<Button variant="secondary" onClick={handleClose}>
								Annuler
							</Button>
							<Button type='submit' variant="success" onClick={handleClose}>
								Ajouter
							</Button>
						</Modal.Footer>
					</form>
  			</Modal>
        </>
    )
}

export default AccountForm