import React, {  useContext, useEffect } from 'react'
import AccountContext from '../../context/account/accountContext'
import AuthContext from '../../context/auth/authContext'
import Credits from '../accounting/credits/Credits'
import Debits from '../accounting/debits/Debits'

const Account = props => {
    const authContext = useContext(AuthContext)
    const accountContext = useContext(AccountContext)

    const { account, getAccount, loading } = accountContext
    const id = props.match.params.id
    useEffect(() => {
        authContext.loadUser()
        getAccount(id)
        // eslint-disable-next-line
    }, [])
    
   
    return (
        <div>
            { account && !loading ? (
            <>
                <h4 className='text-center'>{account.name.toUpperCase()}</h4>
                <div className='row'>
                    <div className='col-lg'>
                        <Credits accountId={id} />
                    </div>
                    <div className='col-lg'>
                        <Debits accountId={id} />
                    </div>
                </div>
                <hr />
            </>
            ): null }
        </div>
    )
}

export default Account