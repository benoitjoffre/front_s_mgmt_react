import React, { Fragment, useContext, useEffect } from 'react'
import DebitContext from '../../../context/debit/debitContext'
import DebitItem from '../debits/DebitItem'
import Spinner from '../../layout/Spinner'
import DebitForm from './DebitForm'

const Debits = accountId => {
  const debitContext = useContext(DebitContext)

  const { debits, getDebits, loading, clearDebits } = debitContext
  
  useEffect(() => {
    clearDebits()
    getDebits()
    // eslint-disable-next-line
  },[])
  
  if(debits !== null && debits.length === 0 && !loading) {
    return (
      <>
    <h5 className='text-center' style={{marginTop: '50px'}}>Vous n'avez pas de débits</h5>
    <DebitForm accountId={accountId.accountId}/>
    </>
    )
    
  }

  const AccountId = parseFloat(accountId.accountId)


  // Montant total par account
  const totalAmount = debits && debits.map(debit => (
    debit.account_id === AccountId ? parseFloat(debit.amount) : null
  ))
  .reduce((acc, value) => acc + value, 0)
  debits && debits.map(debit => 
    debit.account_id === parseFloat(accountId.accountId) ? totalAmount : null
  )


  // Séparer par milliers centaines...
  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  }

  return (
    <Fragment>
      <h6 className='text-center'>DÉBITS</h6>
    {debits && !loading ? ( 
      <>
      <table className='table table-hover' style={{maxWidth:'500px', margin:'auto'}}>
        <tbody>
        {debits.map(debit => 
          debit.account_id === parseFloat(accountId.accountId) ? <DebitItem key={debit.id} debit={debit} /> : null
          )}
        </tbody>
      </table>
      <table className="table table-hover table-success" style={{maxWidth:'500px', margin:'auto'}}>
        <tbody>
        
          <tr><td className='d-flex justify-content-end'>Montant total : {formatNumber(parseFloat(totalAmount)) } € </td></tr>
        
        </tbody>
      </table>
      <DebitForm accountId={accountId.accountId}/>
        </>
      ) : (
      <Spinner /> 
      )}
    </Fragment>
  )
}

export default Debits