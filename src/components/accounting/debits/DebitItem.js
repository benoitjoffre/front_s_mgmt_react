import React, { Fragment, useContext, useState } from 'react'
import DebitContext from '../../../context/debit/debitContext'
import {Modal, Button } from 'react-bootstrap'
import PropTypes from 'prop-types';

const DebitItem = ({ debit }) => {
    const debitContext = useContext(DebitContext)

    const { id, desc, amount } = debit

    const { deleteDebit } = debitContext

    const onDelete = () => {
        deleteDebit(id)
        const handleClose = () => setShow(false);
        handleClose()
        // eslint-disable-next-line
    }

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      }

    return (
        <Fragment>
            <tr>
                <td>
                	{desc.toUpperCase()}
                </td>
                <td>
                	{formatNumber(amount)} €
                </td>
                <td>
                    <i onClick={handleShow} className="fas fa-trash-alt"></i>
                </td>

                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                    <Modal.Title>Supprimer un crédit</Modal.Title>
                    </Modal.Header>
                    
                    <Modal.Body>
                                <h4 className='text-center'>Êtes-vous sûr de vouloir supprimer ce crédit ?</h4>
                    </Modal.Body>
                    <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                    Annuler
                            </Button>
                            <Button variant="danger" onClick={onDelete}>
                                    Supprimer
                            </Button>
                    </Modal.Footer>
                </Modal>
            </tr>
        </Fragment>
    )
}

DebitItem.propTypes = {
    debit: PropTypes.object.isRequired
};

export default DebitItem