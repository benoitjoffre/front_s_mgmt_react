import React, { useState, useContext } from 'react'
import DebitContext from '../../../context/debit/debitContext'
import {Modal, Button } from 'react-bootstrap'
import DatePicker from "react-datepicker";
import moment from 'moment'
import "react-datepicker/dist/react-datepicker.css";
const DebitForm = accountId => {
	const debitContext = useContext(DebitContext)

	const { addDebit, getDebits } = debitContext
	const [startDate, setStartDate] = useState(new Date());
    const [debit, setDebit] = useState({
        desc: '',
				amount: '',
				startDate: moment(startDate).format('YYYY-MM-DD')
    });

		const { desc, amount } = debit
		
		const onChange = e => setDebit({ ...debit, [e.target.name]: e.target.value })

		const onSubmit = e => {
			e.preventDefault()
			addDebit(debit, accountId)
			console.log(debit)
      		setDebit({ desc: '', amount: '', startDate: moment().format('YYYY-MM-DD')  })
      		getDebits()
		}

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
		const handleShow = () => setShow(true);
		
  
    return (
        <div className='d-flex justify-content-center flex-column'>
            <Button variant="success" onClick={handleShow} style={{borderRadius: '100px', maxWidth: '500px', margin: '50px auto'}}>
                Ajouter un débit
            </Button>

            <Modal show={show} onHide={handleClose}>
				<Modal.Header closeButton>
				<Modal.Title>Ajouter un crédit</Modal.Title>
				</Modal.Header>
					<form onSubmit={onSubmit}>
						<Modal.Body>
	                        <div className='form-group'>
								<input 
								className='form-control'
								type='text' 
								placeholder='Description'
								name='desc'
								value={desc}
								onChange={onChange}
								/>
	                        </div>

	                        <div className='form-group'>
	                          	<input 
								className='form-control'
								type='text' 
								placeholder='Montant'
								name='amount'
								value={amount.trim()}
								onChange={onChange}
								/>
	                        </div>
							<div className='form-group'>
								<DatePicker
									showPopperArrow={false}
								selected={startDate}
								onChange={date => setStartDate(date)}
								/>
							</div>
						</Modal.Body>
						<Modal.Footer>
							<Button variant="secondary" onClick={handleClose}>
								Annuler
							</Button>
							<Button type='submit' variant="success" onClick={handleClose}>
								Ajouter
							</Button>
						</Modal.Footer>
					</form>
  			</Modal>
        </div>
    )
}

export default DebitForm