import React, { Fragment, useContext, useEffect } from 'react'
import CreditContext from '../../../context/credit/creditContext'
import CreditItem from '../credits/CreditItem'
import Spinner from '../../layout/Spinner'
import CreditForm from './CreditForm'
const Credits = accountId => {
  const creditContext = useContext(CreditContext)

  const { credits, getCredits, loading, clearCredits } = creditContext
  
  useEffect(() => {
    clearCredits()
    getCredits()
    // eslint-disable-next-line
  },[])
  
  if(credits !== null && credits.length === 0 && !loading) {
    return (
      <>
    <h5 className='text-center' style={{marginTop: '50px'}}>Vous n'avez pas de crédits</h5>
    <CreditForm accountId={accountId.accountId}/>
    </>
    )
    
  }

  const AccountId = parseFloat(accountId.accountId)


  // Montant total par account
  const totalAmount = credits && credits.map(credit => (
    credit.account_id === AccountId ? parseFloat(credit.amount) : null
  ))
  .reduce((acc, value) => acc + value, 0)
  credits && credits.map(credit => 
    credit.account_id === parseFloat(accountId.accountId) ? totalAmount : null
  )


  // Séparer par milliers centaines...
  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  }

  return (
    <Fragment>
      <h6 className='text-center'>CRÉDITS</h6>
    {credits && !loading ? ( 
      <>
      <table className='table table-hover' style={{maxWidth:'500px', margin:'auto'}}>
        <tbody>
        {credits.map(credit => 
          credit.account_id === parseFloat(accountId.accountId) ? <CreditItem key={credit.id} credit={credit} /> : null
          )}
        </tbody>
      </table>
      <table className="table table-hover table-success" style={{maxWidth:'500px', margin:'auto'}}>
        <tbody>
        
          <tr><td className='d-flex justify-content-end'>Montant total : {formatNumber(parseFloat(totalAmount)) } € </td></tr>
        
        </tbody>
      </table>
      <CreditForm accountId={accountId.accountId}/>
        </>
      ) : (
      <Spinner /> 
      )}
    </Fragment>
  )
}

export default Credits