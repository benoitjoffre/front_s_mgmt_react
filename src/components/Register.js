import React, { useState, useContext, useEffect } from 'react'
import AlertContext from '../../src/context/alert/alertContext' 
import AuthContext from '../../src/context/auth/authContext'
import '../assets/sass/authForm.scss'


const Register = (props) => {
  const alertContext = useContext(AlertContext)
  const authContext = useContext(AuthContext)

  const { setAlert } = alertContext

  const { register, error, clearErrors, isAuthenticated } = authContext


  useEffect(() => {

    if(isAuthenticated) {
      props.history.push('/ma-startup')
      setAlert('Vous êtes inscrit !', 'success')
    }

    if(error) {
      setAlert(error, 'danger')
      clearErrors()
    }
  }, [clearErrors, error, isAuthenticated, props.history, setAlert])
  const [user, setUser] = useState({
    name: '',
    email: '',
    password: '',
    password2: ''
  })

  const { name, email, password, password2 } = user

  const onChange = e => setUser({...user, [e.target.name]: e.target.value})

  const onSubmit = (e) => {
     e.preventDefault()
     if(name === '' || email === '' || password === '') {
       setAlert('Veuillez remplir tous les champs !', 'danger')
     } else if (password.length < 6) {
        setAlert('Le mot de passe doit contenir au moins 6 caractères !', 'danger')
     } else if (password !== password2) {
       setAlert('Les mots de passe ne correspondent pas', 'danger')
     } 
    else {
       register({
         name,
         email, 
         password
       })
    }
  }

    return(
        <>
          <h1 className='text-center'>Je m'enregistre</h1>
          <form onSubmit={onSubmit} className='register-form'>
          <div className="form-group">
            <label htmlFor='name'>Nom complet :</label>
            <input className='form-control' id='name' type='text' name='name'  value={name} onChange={onChange} />
          </div>

          <div className="form-group">
            <label htmlFor='email'>Email :</label>
            <input className='form-control' id='email' type='email'  name='email' value={email} onChange={onChange} />
          </div> 
              
          <div className="form-group">
            <label htmlFor='password'>Mot de passe :</label>
            <input className='form-control' id='password' type='password'  name='password' value={password} onChange={onChange} />
          </div>

          <div className="form-group">
            <label htmlFor='password2'>Confirmation du mot de passe :</label>
            <input className='form-control' id='password2' type='password'  name='password2' value={password2} onChange={onChange} />
          </div>
              
              <button className='btn btn-success' type='submit'>Valider</button>
          </form>
        </>
    )
}
export default Register;