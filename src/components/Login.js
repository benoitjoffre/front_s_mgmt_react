import React, { useState, useContext, useEffect } from 'react'
import AuthContext from '../../src/context/auth/authContext'
import AlertContext from '../../src/context/alert/alertContext' 
import '../assets/sass/authForm.scss'

const Login = (props) => {
  const alertContext = useContext(AlertContext)
  const authContext = useContext(AuthContext)

  const { setAlert } = alertContext

  const { login, error, clearErrors, isAuthenticated } = authContext

  useEffect(() => {
    if(isAuthenticated) {
      props.history.push('/ma-startup')
      setAlert('Vous êtes connecté', 'success')
    }
    if(error) {
      setAlert(error, 'danger')
      clearErrors()
    }
  }, [clearErrors, error, isAuthenticated, props.history, setAlert])


  const [user, setUser] = useState({
    email: '',
    password: ''
  })

  const { email, password } = user

  const onChange = e => setUser({...user, [e.target.name]: e.target.value})

  
  const onSubmit = (e) => {
    e.preventDefault()
    if(email === '' || password === '') {
      setAlert('Veuillez remplir tous les champs', 'danger')
    } else {
      login({
        email, password
      })
    }
  }

    return(
      <>
        <h1 className='text-center'>Je me connecte</h1>
        <form onSubmit={onSubmit} className='register-form'>

          <div className="form-group">
            <label htmlFor='email'>Email :</label>
            <input className='form-control' id='email' type='email'  name='email' value={email} onChange={onChange} />
          </div>
            
          <div className="form-group">
            <label htmlFor='password'>Mot de passe :</label>
            <input className='form-control' id='password' type='password'  name='password' value={password} onChange={onChange} />
          </div>
            
          <button className='btn btn-success' type='submit'>Valider</button>
        </form>
      </>
    )
}
export default Login;