import React, { Fragment, useContext } from 'react';
import { Link } from 'react-router-dom';
import '../../assets/sass/navbar.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faRocket } from '@fortawesome/free-solid-svg-icons'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import AuthContext from '../../context/auth/authContext' 
const Navbar = () => {
    const authContext = useContext(AuthContext)

    const { isAuthenticated, logout, user } = authContext

    const onLogout = () => {
        logout()
    }

    const authLinks = (
        <Fragment>
            <Link to='/ma-startup'>Ma Startup</Link> {' '}
            Bienvenue { user && user.name } &nbsp; &nbsp;
            <a onClick={onLogout} href="#!">
            <FontAwesomeIcon icon={faSignOutAlt} /> {' '}<span className="hide-sm">Se deconnecter</span>
            </a>
        </Fragment>
    )

    const guestLinks = (
        <Fragment>
           <Link to='/login'> Se connecter</Link>
            <Link to='/register'> S'enregistrer</Link>
        </Fragment>
    )

    return (
    <div className='nav'>
        <span className="logo">
            <i className="fas fa-rocket fa-2x"></i>
            <Link to="/" className="name">StartupManagement</Link>
        </span>
        <span>
            { isAuthenticated ? authLinks : guestLinks }
        </span>
    </div>
    )
}
export default Navbar