import React, { Fragment } from 'react'
import { Spinner } from 'react-bootstrap'

export default () => (
    <Fragment>
        <div className='d-flex justify-content-center align-items-center'>
        <Spinner animation="border" variant="success" size='lg' style={{width: '100px', height:'100px'}} />
        </div>
    </Fragment>
)