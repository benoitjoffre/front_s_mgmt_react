import React, { useContext, useEffect } from 'react'
import AuthContext from '../context/auth/authContext'
import Accounts from './accounting/Accounts'
import AccountForm from './accounting/AccountForm'
const Home = () => {
    const authContext = useContext(AuthContext)

    useEffect(() => {
        authContext.loadUser()
        // eslint-disable-next-line
    }, [])

    return (
        <>
        	<div className="card d-flex justify-content-center align-items-center" style={{width: '90%', height: '250px', margin: 'auto'}}>
						GRAPH
			</div>
            
            <span className='d-flex justify-content-end'>
                <AccountForm />
            </span>
            <Accounts />
        </>
    )
}

export default Home