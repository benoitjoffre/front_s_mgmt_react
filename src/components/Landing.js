import React from 'react'
import LandingIMG from '../assets/img/landing.png'

const Landing = () => {

    return (
        <div className="d-flex align-items-center flex-column justify-content-center">
	        <h1 className="">L'application pensée pour les startups</h1>
	        <img src={LandingIMG} width="550" height="450" alt=""/>
        </div>
    )
}

export default Landing