import './assets/bootstrap-theme/bootstrap.min.css';

import React from 'react';
import Navbar from './components/layout/Navbar'
import Login from './components/Login.js'
import Register from './components/Register.js'
import Landing from './components/Landing.js'
import Home from './components/Home.js'
import Account from './components/accounting/Account'
import PrivateRoute from './route/PrivateRoute'
import Alerts from './components/layout/Alerts'
import AuthState from './context/auth/authState'
import AlertState from './context/alert/alertState'
import AccountState from './context/account/accountState'
import CreditState from './context/credit/creditState'
import DebitState from './context/debit/debitState'
import setAuthToken from './utils/setAuthToken'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import './App.css'


if(localStorage.token) {
  setAuthToken(localStorage.token)
}

const App = () => {

  return (
    <AuthState>
      <AccountState>
        <CreditState>
        <DebitState>
          <AlertState>
            <Router>
                <Navbar />
                <br />
                <Alerts />
                {/* Routing */}
                <Switch>
                    <Route exact path="/" component={Landing} />
                    <Route path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                    <PrivateRoute path="/ma-startup" component={Home} />
                    <PrivateRoute path="/mon-budget/:id" component={Account} />
                </Switch>
            </Router>
          </AlertState>
          </DebitState>
        </CreditState>
      </AccountState>
    </AuthState>
  );

}

export default App