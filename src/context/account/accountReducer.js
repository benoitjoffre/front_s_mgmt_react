import {
    ADD_ACCOUNT,
    DELETE_ACCOUNT,
    ACCOUNT_ERROR,
    GET_ACCOUNT,
    GET_ACCOUNTS,
    CLEAR_ACCOUNT
} from '../types'


export default (state, action) => {
  switch(action.type) {
    case GET_ACCOUNT:
      return {
        ...state,
        account: action.payload,
        loading: false
      }
    case CLEAR_ACCOUNT:
      return {
        ...state,
        account: null
      };
    case GET_ACCOUNTS:
      return {
        ...state,
        accounts: action.payload, 
        loading: false
      }
		case ADD_ACCOUNT:
			return {
				...state,
        accounts: [action.payload, ...state.accounts],
        loading: false
      }
    
    case DELETE_ACCOUNT:
      return {
        ...state,
        accounts: state.accounts.filter(account => account.id !== action.payload),
        loading: false
      }
    case ACCOUNT_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}