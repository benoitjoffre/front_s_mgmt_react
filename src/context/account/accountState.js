import React, { useReducer } from 'react'
import axios from 'axios'
import AccountContext from './accountContext'
import accountReducer from './accountReducer'

import {
    ADD_ACCOUNT,
    DELETE_ACCOUNT,
		ACCOUNT_ERROR,
		GET_ACCOUNTS,
		GET_ACCOUNT,
		CLEAR_ACCOUNT
} from '../types'


const AccountState = props => {
	const initialState = {
		account: null,
		accounts: null,
		error: null
	}

	const [state, dispatch] = useReducer(accountReducer, initialState)

	// get Account
	const getAccount = async id => {
		try {
			
			// console.log('id budget', id)
			const res = await axios.get('http://startup-management-api.local/api/accounts/'+ id)
			dispatch({
				type: GET_ACCOUNT,
				payload: res.data
			})
		} catch (err) {
			dispatch({
				type: ACCOUNT_ERROR,
				payload: err
			})
		}
	}
	// get Accounts 
	const getAccounts = async () => {
		try {
			const res = await axios.get('http://startup-management-api.local/api/accounts')
			dispatch({ 
				type: GET_ACCOUNTS, 
				payload: res.data
			})
		} catch (err) {
			dispatch({
				type: ACCOUNT_ERROR,
				payload: err
			})
		}
	}

	// Add account
	const addAccount = async account => {
		
		const config = {
			headers: {
				'Content-Type': 'application/json',
			}
		}
		try {
			const res = await axios.post('http://startup-management-api.local/api/accounts', account, config)
			dispatch({ 
				type: ADD_ACCOUNT,
				payload: res.data 
			})
		} catch (err) {
			dispatch({
				type: ACCOUNT_ERROR,
				payload: err.response.message
			})
		}
	}

	// delete account
	const deleteAccount = async id => {
		try {
			await axios.delete('http://startup-management-api.local/api/accounts/'+id)

			dispatch({ type: DELETE_ACCOUNT, payload: id })
		} catch (err) {
			dispatch({
				type: ACCOUNT_ERROR,
				payload: err.response.message
			})
		}
	}

	// clear accounts
	const clearAccounts = () => {
		dispatch({ type: CLEAR_ACCOUNT });
	  };

	return (
		<AccountContext.Provider
		value={{
			account: state.account,
			accounts: state.accounts,
			error: state.error,
			getAccount,
			getAccounts,
			addAccount,
			deleteAccount,
			clearAccounts
		}}
		>
			 { props.children }
		</AccountContext.Provider>
	)
}

export default AccountState