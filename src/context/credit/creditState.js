import React, { useReducer } from 'react'
import axios from 'axios'
import CreditContext from './creditContext'
import creditReducer from './creditReducer'

import {
    ADD_CREDIT,
    DELETE_CREDIT,
    CREDIT_ERROR,
    GET_CREDITS,
    CLEAR_CREDITS
} from '../types'

const CreditState = props => {

    const initialState = {
		credits: null,
		error: null
	}

    // get Credits
	const getCredits = async () => {
		try {
			const params = {
				headers: {
					token: localStorage.getItem('token')
				}
			}
			const response = await axios.get('http://startup-management-api.local/api/credits', {params})
			
			dispatch({ 
				type: GET_CREDITS, 
				payload: response.data.credits
			})
		} catch (err) {
			dispatch({
				type: CREDIT_ERROR,
				payload: err
			})
		}
	}
    // add Credit
	const addCredit = async (credit, accountId) => {
		
		const config = {
			headers: {
				'Content-Type': 'application/json'
			}
		}
		try {
			const res = await axios.post('http://startup-management-api.local/api/credits/account/' + parseFloat(accountId.accountId), credit, config)
			dispatch({ 
				type: ADD_CREDIT,
				payload: res.data 
			})
		} catch (err) {
			dispatch({
				type: CREDIT_ERROR,
				payload: err
			})
		}
	}
    // delete Credit
    const deleteCredit = async id => {
		try {
			await axios.delete('http://startup-management-api.local/api/credits/'+id)

			dispatch({ type: DELETE_CREDIT, payload: id })
		} catch (err) {
			dispatch({
				type: CREDIT_ERROR,
				payload: err.response.message
			})
		}
	}
    // clear Credits
    const clearCredits = () => {
    	dispatch({ type: CLEAR_CREDITS });
    }
	const [state, dispatch] = useReducer(creditReducer, initialState)
    return (
		<CreditContext.Provider
		value={{
			credits: state.credits,
			error: state.error,
			getCredits,
			addCredit,
			deleteCredit,
			clearCredits
		}}
		>
			 { props.children }
		</CreditContext.Provider>
	)
}

export default CreditState



