import {
    ADD_CREDIT,
    DELETE_CREDIT,
    CREDIT_ERROR,
    GET_CREDITS,
    CLEAR_CREDITS
} from '../types'


export default (state, action) => {
  switch(action.type) {
    
    case GET_CREDITS:
      return {
        ...state,
        credits: action.payload, 
        loading: false
      }
		case ADD_CREDIT:
			return {
				...state,
        credits: [action.payload, ...state.credits],
        loading: false
      }
    
    case DELETE_CREDIT:
      return {
        ...state,
        credits: state.credits.filter(credit => credit.id !== action.payload),
        loading: false
			}
		case CLEAR_CREDITS:
      return {
        ...state,
        credits: null, 
        error: null,
      };
    case CREDIT_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}