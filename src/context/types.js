export const GET_ACCOUNTS = 'GET_ACCOUNTS'
export const GET_ACCOUNT = 'GET_ACCOUNT'
export const CLEAR_ACCOUNT = 'CLEAR_ACCOUNT'
export const ADD_ACCOUNT = 'ADD_ACCOUNT'
export const DELETE_ACCOUNT = 'DELETE_ACCOUNT'
export const UPDATE_ACCOUNT = 'UPDATE_ACCOUNT'
export const ACCOUNT_ERROR = 'ACCOUNT_ERROR'

export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAIL = 'REGISTER_FAIL'
export const USER_LOADED = 'USER_LOADED'
export const AUTH_ERROR = 'AUTH_ERROR'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGOUT = 'LOGOUT'
export const CLEAR_ERRORS = 'CLEAR_ERRORS'

export const SET_ALERT = 'SET_ALERT'
export const REMOVE_ALERT = 'REMOVE_ALERT'

export const GET_CREDITS = 'GET_CREDITS'
export const CLEAR_CREDITS = 'CLEAR_CREDITS'
export const ADD_CREDIT = 'ADD_CREDIT'
export const DELETE_CREDIT = 'DELETE_CREDIT'
export const UPDATE_CREDIT = 'UPDATE_CREDIT'
export const CREDIT_ERROR = 'CREDIT_ERROR'

export const GET_DEBITS = 'GET_DEBITS'
export const CLEAR_DEBITS = 'CLEAR_DEBITS'
export const ADD_DEBIT = 'ADD_DEBIT'
export const DELETE_DEBIT = 'DELETE_DEBIT'
export const UPDATE_DEBIT = 'UPDATE_DEBIT'
export const DEBIT_ERROR = 'DEBIT_ERROR'

export default (state, action) => {
    switch(action.type) {
        case REGISTER_SUCCESS:
        localStorage.setItem('token', action.payload.token)
        return {
            ...state,
            ...action.payload,
            isAuthenticated: true,
            loading: false
        }
        case REGISTER_FAIL:
        localStorage.removeItem('token')
        return {
            ...state,
            token: null,
            isAuthenticated: false,
            loading: false,
            user: null,
            error: action.payload

        }
        default:
            return state;
    }
}