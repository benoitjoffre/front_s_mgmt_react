import React, { useReducer } from 'react'
import AuthContext from './authContext'
import AuthReducer from './authReducer'
import setAuthToken from '../../utils/setAuthToken'

import axios from 'axios'
import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_ERRORS
} from '../types'

const AuthState = props => {
  const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated: null,
    loading: true,
    user: null,
    error: null
  }

  const [state, dispatch] = useReducer(AuthReducer, initialState)

  // Load user
  const loadUser = async  () => {
    if(localStorage.token) {
      setAuthToken(localStorage.token)
    }

    try {
      const params = {
        token : localStorage.getItem('token')
      }
      const res = await axios.get('http://startup-management-api.local/api/user', {params})

      dispatch({ 
        type: USER_LOADED, 
        payload: res.data.user
      })
    } catch (err) {
      dispatch({ type: AUTH_ERROR })
    }
  }
  // Register user
  const register = async formData => {
    const config = {
      headers: {
        'Content-Type': 'appplication/json'
      }
    };

    try {
      const res = await axios.post('http://startup-management-api.local/api/register', formData, config)
      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data.token
      })
      loadUser()
    } catch (err) {
      dispatch({
        type: REGISTER_FAIL,
        payload: err.response.data.errors.email
      })
    }
  }
  // Login user
  const login = async formData => {
    const config = {
      headers: {
        'Content-Type': 'appplication/json'
      }
    };

    try {
      const res = await axios.post('http://startup-management-api.local/api/login', formData, config)
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data.token
      })
      loadUser()
    } catch (err) {
      dispatch({
        type: LOGIN_FAIL,
        payload: err.response.data.message
      })
    }
  }

  // Logout
  const logout = async  () => dispatch({type: LOGOUT})

  // Clear errors
  const clearErrors = async  () => dispatch({type: CLEAR_ERRORS})

  return (
    <AuthContext.Provider
      value={{
        token: state.token,
        isAuthenticated: state.isAuthenticated,
        loading: state.loading,
        user: state.user,
        error: state.error,
        register,
        loadUser,
        login,
        logout,
        clearErrors
      }}
    >
      {props.children}
    </AuthContext.Provider>
  )
}

export default AuthState