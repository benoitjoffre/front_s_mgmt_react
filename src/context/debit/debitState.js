import React, { useReducer } from 'react'
import axios from 'axios'
import DebitContext from './debitContext'
import debitReducer from './debitReducer'

import {
    ADD_DEBIT,
    DELETE_DEBIT,
    DEBIT_ERROR,
    GET_DEBITS,
    CLEAR_DEBITS
} from '../types'

const DebitState = props => {

    const initialState = {
		debits: null,
		error: null
	}

    // get Debits
	const getDebits = async () => {
		try {
			const params = {
				headers: {
					token: localStorage.getItem('token')
				}
			}
			const response = await axios.get('http://startup-management-api.local/api/debits', {params})
			
			dispatch({ 
				type: GET_DEBITS, 
				payload: response.data.debits
			})
		} catch (err) {
			dispatch({
				type: DEBIT_ERROR,
				payload: err
			})
		}
	}
    // add Debit
	const addDebit = async (debit, accountId) => {
		
		const config = {
			headers: {
				'Content-Type': 'application/json'
			}
		}
		try {
			const res = await axios.post('http://startup-management-api.local/api/debits/account/' + parseFloat(accountId.accountId), debit, config)
			dispatch({ 
				type: ADD_DEBIT,
				payload: res.data 
			})
		} catch (err) {
			dispatch({
				type: DEBIT_ERROR,
				payload: err
			})
		}
	}
    // delete Debit
    const deleteDebit = async id => {
		try {
			await axios.delete('http://startup-management-api.local/api/debits/'+id)

			dispatch({ type: DELETE_DEBIT, payload: id })
		} catch (err) {
			dispatch({
				type: DEBIT_ERROR,
				payload: err.response.message
			})
		}
	}
    // clear Debits
    const clearDebits = () => {
    	dispatch({ type: CLEAR_DEBITS });
    }
	const [state, dispatch] = useReducer(debitReducer, initialState)
    return (
		<DebitContext.Provider
		value={{
			debits: state.debits,
			error: state.error,
			getDebits,
			addDebit,
			deleteDebit,
			clearDebits
		}}
		>
			 { props.children }
		</DebitContext.Provider>
	)
}

export default DebitState