import {
    ADD_DEBIT,
    DELETE_DEBIT,
    DEBIT_ERROR,
    GET_DEBITS,
    CLEAR_DEBITS
} from '../types'


export default (state, action) => {
  switch(action.type) {
    
    case GET_DEBITS:
      return {
        ...state,
        debits: action.payload, 
        loading: false
      }
		case ADD_DEBIT:
			return {
				...state,
        debits: [action.payload, ...state.debits],
        loading: false
      }
    
    case DELETE_DEBIT:
      return {
        ...state,
        debits: state.debits.filter(debit => debit.id !== action.payload),
        loading: false
			}
		case CLEAR_DEBITS:
      return {
        ...state,
        debits: null, 
        error: null,
      };
    case DEBIT_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}